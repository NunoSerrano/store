﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Store.Web.Data.Entities;

namespace Store.Web.Data
{
    public class SeedDb
    {

        private readonly DataContext context;

        private Random random;

        public SeedDb(DataContext context)
        {
            this.context = context;
            this.random = new Random();
        }

        public async Task SeedAsync()
        {
            //verifica se existe BD
            await this.context.Database.EnsureCreatedAsync();

            if (!this.context.Products.Any())
            {
                this.AddProduct("Equipamento oficial do SCP");
                this.AddProduct("Chuteiras oficiais");
                this.AddProduct("Leão pequeno");
                await this.context.SaveChangesAsync();
            }



        }

        private void AddProduct(string name)
        {
            this.context.Products.Add(new Product
            {
                Name = name,
                Price = this.random.Next(200),
                IsAvailable = true,
                Stock = this.random.Next(100)
            }
            );
        }
    }
}
