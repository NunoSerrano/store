﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Store.Web.Data.Entities;


namespace Store.Web.Data
{
    public class DataContext: DbContext
    {
        public DbSet<Product> Products{ get; set; }


        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }



    }
}
